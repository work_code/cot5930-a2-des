#include <stdio.h>
#include <string.h>
#include "lib/sha1.h"
#include "des.h"

// Generates the 48-bit subkeys needed for the rounds of encryption or decryption. The
// decryption flag should be set to 0 to generate encryption keys and non-zero to generate
// decryption keys.
void generate_subkeys(unsigned char masterkey[], unsigned char subkeys[16][6], int decryption_flag) {
    unsigned int c = 0;
    unsigned int d = 0;

    // Calculate C using the first 4 rows of PC-1.
    for (int i = 0; i < 28; i++) {
        int byte_position = ((PC1[i] - 1) / 8);         // Determine which byte is needed from the master key.
        int bit_position = (7 - ((PC1[i] - 1) % 8));    // Determine which bit is needed from that byte.
        // Shift the bit right so it's the least significant and mask it to get it's value,
        // then shift the value to the correct place in C, building from left to right, OR-ing it with the result.
        c |= ((masterkey[byte_position] >> bit_position) & 0x01) << (32 - i - 1);
    }

    // Calculate D using the last 4 rows of PC-1.
    for (int i = 0; i < 28; i++) {
        int byte_position = ((PC1[i + 28] - 1) / 8);        // Determine which byte is needed from the master key.
        int bit_position = (7 - ((PC1[i + 28] - 1) % 8));   // Determine which bit is needed from that byte.
        // Shift the bit right so it's the least significant and mask it to get it's value,
        // then shift the value to the correct place in D, building from left to right, OR-ing it with the result.
        d |= ((masterkey[byte_position] >> bit_position) & 0x01) << (32 - i - 1);
    }

    // Generate keys
    for (int i = 0; i < 16; i++) {
        int round = i + 1;

        if (decryption_flag == 0) { // Perform shifts for encryption.
            if (round == 1 || round == 2 || round == 9 || round == 16) {
                // Rotate C and D left by 1 bit -> shift left by 1, shift right 28 bits - 1, OR the result and mask
                // the last 4 bits.
                c = ((c << 1) | (c >> (28 - 1))) & 0xfffffff0;
                d = ((d << 1) | (d >> (28 - 1))) & 0xfffffff0;
            } else {
                // Rotate C and D left by 2 bit -> shift left by 2, shift right 28 bits - 2, OR the result and mask
                // the last 4 bits.
                c = ((c << 2) | (c >> (28 - 2))) & 0xfffffff0;
                d = ((d << 2) | (d >> (28 - 2))) & 0xfffffff0;
            }
        } else { // Perform shifts for decryption.
            if (round == 2 || round == 9 || round == 16) {
                // Rotate C and D left by 1 bit -> shift right by 1, shift left 28 bits - 1, OR the result and mask
                // the last 4 bits.
                c = ((c >> 1) | (c << (28 - 1))) & 0xfffffff0;
                d = ((d >> 1) | (d << (28 - 1))) & 0xfffffff0;
            } else if (round != 1) {
                // Rotate C and D left by 2 bit -> shift left by 2, shift right 28 bits - 2, OR the result and mask
                // the last 4 bits.
                c = ((c >> 2) | (c << (28 - 2))) & 0xfffffff0;
                d = ((d >> 2) | (d << (28 - 2))) & 0xfffffff0;
            }
        }

        for (int n = 0; n < 6; n++) {
            subkeys[i][n] = 0;
        }

        // Generate the 48-bit subkeys using shifted C and D along with the PC-2 permutation table.
        for (int j = 0; j < 48; j++) {
            int byte_position = j / 8;
            int bit_position = 7 - (j % 8);
            if (j < 24) {
                subkeys[i][byte_position] |= ((c >> (31 - (PC2[j] - 1))) & 0x00000001) << bit_position;
            } else {
                subkeys[i][byte_position] |= ((d >> (31 - (PC2[j] - 1 - 28))) & 0x00000001) << bit_position;
            }
        }
    }
}

unsigned int function_f(unsigned int right, unsigned char key[]) {
    uint64_t _right = 0;
    uint64_t _key = 0;

    for (int i = 0; i < 48; i++) {
        int bit_source = i;
        int bit_destination = E[i] - 1;
        unsigned int bit = (right >> bit_source) & 0x00000001;
        _right |= (bit << bit_destination) & 0x000000ffffffffffff;
    }

    for (int i = 0; i < 48; i++) {
        int byte_position = i / 8;
        int bit_position = 7 - ((i - 1) % 8);
        int key_bit = (key[byte_position] >> bit_position) & 0x000000ff;
        _key |= (key_bit << (48 - i - 1)) & 0x000000ffffffffffff;
    }

    uint64_t sbox_input = _right ^ _key;
    unsigned int sbox_output = 0;
    unsigned int _block1 = (unsigned int)((sbox_input & 0xfc0000000000) >> 42);
    unsigned int _block2 = (unsigned int)((sbox_input & 0x03f000000000) >> 36);
    unsigned int _block3 = (unsigned int)((sbox_input & 0x000fc0000000) >> 30);
    unsigned int _block4 = (unsigned int)((sbox_input & 0x00003f000000) >> 24);
    unsigned int _block5 = (unsigned int)((sbox_input & 0x000000fc0000) >> 18);
    unsigned int _block6 = (unsigned int)((sbox_input & 0x00000003f000) >> 12);
    unsigned int _block7 = (unsigned int)((sbox_input & 0x000000000fc0) >>  6);
    unsigned int _block8 = (unsigned int)((sbox_input & 0x00000000003f) >>  0);

    int row = 0;
    int col = 0;

    row = ((_block1 & 0x20) >> 5) | (_block1 & 0x01);
    col = _block1 & 0x1e;
    _block1 = SBOX1[row][col];

    row = ((_block2 & 0x20) >> 5) | (_block2 & 0x01);
    col = _block2 & 0x1e;
    _block2 = SBOX2[row][col];

    row = ((_block3 & 0x20) >> 5) | (_block3 & 0x01);
    col = _block3 & 0x1e;
    _block3 = SBOX3[row][col];

    row = ((_block4 & 0x20) >> 5) | (_block4 & 0x01);
    col = _block4 & 0x1e;
    _block4 = SBOX4[row][col];

    row = ((_block5 & 0x20) >> 5) | (_block5 & 0x01);
    col = _block5 & 0x1e;
    _block5 = SBOX5[row][col];

    row = ((_block6 & 0x20) >> 5) | (_block6 & 0x01);
    col = _block6 & 0x1e;
    _block6 = SBOX6[row][col];

    row = ((_block7 & 0x20) >> 5) | (_block7 & 0x01);
    col = _block7 & 0x1e;
    _block7 = SBOX7[row][col];

    row = ((_block8 & 0x20) >> 5) | (_block8 & 0x01);
    col = _block8 & 0x1e;
    _block8 = SBOX8[row][col];

    sbox_output = (_block1 << 28) | (_block2 << 24) | (_block3 << 20) | (_block4 << 16) |
                  (_block5 << 12) | (_block6 <<  8) | (_block7 <<  4) | (_block8 <<  0);

    unsigned int result = 0;

    for (int i = 0; i < 32; i++) {
        int bit_source = i;
        int bit_destination = P[i] - 1;
        unsigned int bit = (sbox_output >> bit_source) & 0x00000001;
        result |= (bit << bit_destination) & 0xffffffff;
    }

    return result;
}

void execute_des(unsigned char block[], unsigned char subkey[][6], unsigned char output[]) {
    unsigned int left = 0; // 0
    unsigned int right = 0;  // 1

    // // Calculate L using the first 4 rows of the initial permutation.
    // for (int i = 0; i < 32; i++) {
    //     int byte_position = ((IP[i] - 1) / 8);         // Determine which byte is needed from the block.
    //     int bit_position = (7 - ((IP[i] - 1) % 8));    // Determine which bit is needed from that byte.
    //     // Shift the bit right so it's the least significant and mask it to get it's value,
    //     // then shift the value to the correct place in L, building from left to right, OR-ing it with the result.
    //     left |= ((block[byte_position] >> bit_position) & 0x01) << (32 - i - 1);
    // }
    // // Calculate R using the last 4 rows of the initial permutation.
    // for (int i = 0; i < 32; i++) {
    //     int byte_position = ((PC1[i + 32] - 1) / 8);        // Determine which byte is needed from the master key.
    //     int bit_position = (7 - ((PC1[i + 32] - 1) % 8));   // Determine which bit is needed from that byte.
    //     // Shift the bit right so it's the least significant and mask it to get it's value,
    //     // then shift the value to the correct place in R, building from left to right, OR-ing it with the result.
    //     right |= ((block[byte_position] >> bit_position) & 0x01) << (32 - i - 1);
    // }

    // Calculate L using the first 4 rows of the initial permutation.
    for (int i = 0; i < 32; i++) {
        int byte_position = ((i - 1) / 8);         // Determine which byte is needed from the block.
        int bit_position = (7 - ((i + 1 - 1) % 8));    // Determine which bit is needed from that byte.
        // Shift the bit right so it's the least significant and mask it to get it's value,
        // then shift the value to the correct place in L, building from left to right, OR-ing it with the result.
        left |= ((block[byte_position] >> bit_position) & 0x01) << (32 - i - 1);
    }
    // Calculate R using the last 4 rows of the initial permutation.
    for (int i = 0; i < 32; i++) {
        int byte_position = ((i + 32 - 1) / 8);        // Determine which byte is needed from the master key.
        int bit_position = (7 - ((i + 1 + 32 - 1) % 8));   // Determine which bit is needed from that byte.
        // Shift the bit right so it's the least significant and mask it to get it's value,
        // then shift the value to the correct place in R, building from left to right, OR-ing it with the result.
        right |= ((block[byte_position] >> bit_position) & 0x01) << (32 - i - 1);
    }

    // 16 rounds.
    unsigned int temp = 0;
    for (int i = 0; i < 15; i++) {
        temp = right;
        right = function_f(right, subkey[i]) ^ left;
        left = temp;
    }
    left = function_f(right, subkey[15]) ^ left;

    output[0] = 0;
    output[1] = 0;
    output[2] = 0;
    output[3] = 0;
    output[4] = 0;
    output[5] = 0;
    output[6] = 0;
    output[7] = 0;


    output[0] = (unsigned char)((left & 0xff000000) >> 24);
    output[1] = (unsigned char)((left & 0x00ff0000) >> 16);
    output[2] = (unsigned char)((left & 0x0000ff00) >>  8);
    output[3] = (unsigned char)((left & 0x000000ff) >>  0);
    output[4] = (unsigned char)((right & 0xff000000) >> 24);
    output[5] = (unsigned char)((right & 0x00ff0000) >> 16);
    output[6] = (unsigned char)((right & 0x0000ff00) >>  8);
    output[7] = (unsigned char)((right & 0x000000ff) >>  0);
}

// Reads a line of input from the user up a maximum number of characters. The
// function does not skip whitespace and stops reading at the first new line
// character. It returns the number of characters that are stored in str.
int read_line(unsigned char str[], int max_length) {
    unsigned char c;
    int i = 0;

    while ((c = (unsigned char)getchar()) != '\n') {
        if (i < max_length) {
            str[i++] = c;
        }
    }
    str[i] = '\0';
    return i;
}

int main() {
    unsigned char plaintext[PLAINTEXT_MAX_LENGTH];
    unsigned char password[PASSWORD_MAX_LENGTH];
    unsigned char masterkey[MASTER_KEY_SIZE];
    unsigned char encryption_subkeys[16][6];    // 16 subkeys, each 6 bytes.
    unsigned char decryption_subkeys[16][6];
    unsigned char output[8];
    unsigned char output2[8];

    // Read in the plaintext and password.
    printf("Plaintext: ");
    read_line(plaintext, PLAINTEXT_MAX_LENGTH);
    printf("Password: ");
    read_line(password, PASSWORD_MAX_LENGTH);

    // Calculate the SHA-1 hash of the password and use the first 8 bytes as
    // the master key.
    char buf[SHA1_DIGEST_SIZE];
    sha1_buffer((char*)password, strlen((char*)password), buf);
    for (int i = 0; i < MASTER_KEY_SIZE; i++) {
        masterkey[i] = (unsigned char)buf[i];
    }

    // Print the plaintext and master key in hex.
    printf("\nPlaintext:\n");
    for (int i = 0; i < strlen((char*)plaintext); i++) {
        printf("%02x ", plaintext[i]);
    }
    printf("\n");
    printf ("\nMaster key:\n");
    for (int i = 0; i < MASTER_KEY_SIZE; i++) {
        printf("%02x ", masterkey[i] & 0xff);
    }
    printf("\n");

    generate_subkeys(masterkey, encryption_subkeys, 0);
    generate_subkeys(masterkey, decryption_subkeys, 1);

    printf("\nEncryption subkeys:\n");
    for (int i = 0; i < 16; i++) {
        for (int j = 0; j < 6; j++) {
            printf("%02x ", encryption_subkeys[i][j]);
        }
        printf("\n");
    }

    printf("\nDecryption subkeys:\n");
    for (int i = 0; i < 16; i++) {
        for (int j = 0; j < 6; j++) {
            printf("%02x ", decryption_subkeys[i][j]);
        }
        printf("\n");
    }

    execute_des(plaintext, encryption_subkeys, output);

    printf("\nEncryption result:\n");
    for (int i = 0; i < 8; i++) {
        printf("%02x ", output[i]);
    }
    printf("\n");

    execute_des(output, decryption_subkeys, output2);
    printf("\nDecryption result:\n");
    for (int i = 0; i < 8; i++) {
        printf("%02x ", output2[i]);
    }
    printf("\n");

//
//    printf("\nDecryption subkeys:\n");
//    for (int i = 0; i < 16; i++) {
//        for (int j = 0; j < 6; j++) {
//            printf("%02x ", decryption_subkeys[i][j]);
//        }
//        printf("\n");
//    }

    return 0;
}